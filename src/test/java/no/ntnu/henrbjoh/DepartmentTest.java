package no.ntnu.henrbjoh;

import no.ntnu.henrbjoh.exception.RemoveException;
import org.junit.jupiter.api.Test;

public class DepartmentTest {

    Department testDepartment = new Department("testDepartment");

    /**
     * Tests the different functionalities in the remove-method from Department
     * It tests both removing an employee and a patient,removing a person that is not registered in the system
     * and trying to remove a null-object
     */
    @Test
    public void testRemove() {
         Employee employee1 = new Employee("Will","Ferrel","9854");
         Employee employee2 = new Employee("William","Svoren","5436");
         Patient patient1 = new Patient("Henrik","Johnsrud","1234");

         Patient nullPatient = null;


        testDepartment.addPatient(patient1);
        testDepartment.addEmployee(employee1);

        try {
            testDepartment.remove(patient1);
            System.out.println("Personen er fjernet fra systemet");

        } catch (RemoveException e) {
            e.printStackTrace();
            System.out.println(e);
        }

        try {
            testDepartment.remove(employee1);
            System.out.println("Personen er fjernet fra systemet");

        } catch (RemoveException e) {
            e.printStackTrace();
            System.out.println(e);        }

        try {
            testDepartment.remove(employee2);
            System.out.println("Personen er fjernet fra systemet");

        } catch (RemoveException e) {
            e.printStackTrace();
            System.out.println(e);
        }

        try {
            testDepartment.remove(nullPatient);
            System.out.println("Personen er fjernet fra systemet");

        } catch (RemoveException e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }


}