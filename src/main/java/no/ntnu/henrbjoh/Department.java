package no.ntnu.henrbjoh;

import no.ntnu.henrbjoh.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

public class Department {

    private String departmentName;

    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * Creates a new department-object, which includes a list of employees and patients
     * @param departmentName - name of this department
     */
    public Department(String departmentName){
        this.departmentName = departmentName;

        employees = new ArrayList<>();
        patients = new ArrayList<>();

    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    /**
     *  Adding a new employee to the department.
     * @param employee - the new employee
     * @throws IllegalArgumentException if the person already is registered
     */
    public void addEmployee(Employee employee){
        if (!employees.contains(employee)) {
            this.employees.add(employee);
        }
        else {
            throw new IllegalArgumentException(employee.toString() + " finnes allerede i systemet");
        }
    }

    public ArrayList<Patient> getPatients(){
        return patients;
    }

    /**
     *  Adding a new patient to the department.
     * @param patient - the new employee
     * @throws IllegalArgumentException if the person already is registered
     */
    public void addPatient(Patient patient){
        if (!patients.contains(patient)) {
            this.patients.add(patient);
        }
        else {
            throw new IllegalArgumentException(patient.toString() + " finnes allerede i systemet");
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    /**
     * Remove a person from the system-register. If the person does not exist in the register, exception of the
     * type RemoveException will be thrown. It also check if the person-object is equal to null
     * @param person - the person to be removed
     * @throws RemoveException
     */
    public void remove(Person person) throws RemoveException{
        if (person==null){
            throw new RemoveException("Input kan ikke være = null");
        }

        if (patients.contains(person) && person instanceof Patient) {
            patients.remove(person);
        }
        else if (employees.contains(person) && person instanceof Employee){
            employees.remove(person);
        }
        else {
            throw new RemoveException("Personen finnes ikke i registeret");
        }

    }

    /**
     * Compares a given object to this object
     * @param o - object which will be compared to this object
     * @return true if the to objects are the same, false if they are not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(employees, that.employees) &&
                Objects.equals(patients, that.patients);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employees +
                ", patients=" + patients +
                '}';
    }
}