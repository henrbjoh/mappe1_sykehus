package no.ntnu.henrbjoh.healthpersonal.doctor;

import no.ntnu.henrbjoh.Patient;
import no.ntnu.henrbjoh.healthpersonal.doctor.Doctor;

public class Surgeon extends Doctor {

    public Surgeon (String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}
