package no.ntnu.henrbjoh.healthpersonal.doctor;

import no.ntnu.henrbjoh.Employee;
import no.ntnu.henrbjoh.Patient;

public abstract class Doctor extends Employee {

    protected Doctor (String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    /**
     * Set a diagnosis to a given patient
     * @param patient - the patient to get diagnosed
     * @param diagnosis - String with the diagnosis
     */
    abstract void setDiagnosis(Patient patient, String diagnosis);
}
