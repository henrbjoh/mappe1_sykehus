package no.ntnu.henrbjoh;

public interface Diagnosable {
    public void setDiagnosis(String diagnosis);
}
