package no.ntnu.henrbjoh.exception;

public class RemoveException extends Exception{

    private static final long serialVersionUID = 1L;

    /**
     * Exception to be thrown when a problem accures when trying to remove a person from the system
     *
     * @param e - information about the exception
     */
    public RemoveException(final String e){
        super(e);
    }
}
