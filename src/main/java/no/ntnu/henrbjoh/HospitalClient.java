package no.ntnu.henrbjoh;

import no.ntnu.henrbjoh.exception.RemoveException;

import java.util.ArrayList;

public class HospitalClient {
    /**
     * very simple client to test the application
     * @param args
     */
    public static void main(String[]args){

        Hospital Ahus = new Hospital("Ahus");

        HospitalTestData.fillRegisterWithTestData(Ahus);

        ArrayList<Department> departments = Ahus.getDepartments();

        //Fjerne en person som finnes i systemet
        try {
            for (Department dep : departments){
                if (dep.getDepartmentName().equalsIgnoreCase("Akutten")) {
                    ArrayList<Employee> employees = dep.getEmployees();
                    Person person = employees.get(1);
                    dep.remove(employees.get(1));
                    System.out.println(person + " er fjernet fra systemet");
                }
            }
        }catch (RemoveException e){
            System.out.println(e);
        }
        //fjerne en person som IKKE finnes i systemet
        try {
            Patient patient1 = new Patient("Henrik", "Johnsrud", "321");
            for (Department dep : departments){
                if (dep.getDepartmentName().equalsIgnoreCase("Akutten")) {
                    ArrayList<Employee> employees = dep.getEmployees();
                    dep.remove(patient1);
                }
            }

        }catch (RemoveException e){
            System.out.println(e);
        }
    }
}
