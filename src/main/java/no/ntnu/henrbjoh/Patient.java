package no.ntnu.henrbjoh;

public class Patient extends Person implements Diagnosable{

    private String diagnosis = "";

    protected String getDiagnosis(){
        return diagnosis;
    }

    protected Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    public String toString(){
        return super.toString();
    }

    /**
     * diagnosis set by one of the doctors
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}
