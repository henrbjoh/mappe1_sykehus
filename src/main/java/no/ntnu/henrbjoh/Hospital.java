package no.ntnu.henrbjoh;

import java.util.ArrayList;

public class Hospital {

    private final String hospitalName;

    private ArrayList<Department> departsments;

    /**
     * Creates a new hospital-object, with a list of the departments in the given hospital
     *
     * @param hospitalName - name of the new hospital
     */
    public Hospital(String hospitalName){
        this.hospitalName = hospitalName;
        departsments = new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments(){
        return departsments;
    }

    /**
     * Adds a new departement in the list of departments
     * @param department - the new department to be added
     */
    public void addDepartment(Department department){
        this.departsments.add(department);
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departsments=" + departsments +
                '}';
    }
}